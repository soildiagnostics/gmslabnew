# GMSLab Web Project

## Steps to create a development instance (This is for admin notes only)

1. Install Python 3
2. Install Git
3. Create a working folder (GMSWebProject)
4. Create a virtual environment for Python3
    > python3 -m venv gmslab
5. Activate the virutal environment
    > cd gmslab
    > source bin/activate
6. Create a Django project folder and enter it
    > mkdir gmslab
    > cd gmslab
7. Check that the virtual environment python is active
    > which python
      /Users/kdb_uiuc/CloudHome/GMSWebProject/gmslab/bin/python
8. Upgrade pip 
    > pip install --upgrade pip
9. Install Django and psycopg2
    > pip install Django
    > pip install psycopg2
10. Create a requirements.txt file
    > pip freeze > requirements.txt
11. Create the Dockerfile
    > touch Dockerfile
12. Add the Python3 image to the file. (See Dockerfile)
13. Create the docker-compose.yml file and add the topology to it. 
14. Create the django-project
    > django-admin.py startproject gmslab
15. Start the project with 
    > docker-compose build
    > docker-compose up
16. Once the Heroku site is up (gmslab-dev.herokuapp.com), we need to create the postgis extension
    > heroku pg:psql
    (psql) create extension postgis;
    (psql) \q;

# Standard packages to install
## For Heroku
    pipenv
    django-heroku 
    heroku toolbelt
## For Amazon S3
    django-storages
    boto
    Set up new bucket and user with their own access key. 
## For admin work
    django-grappelli
## For Development work 
    django-toolbar
    django-extensions
## For application development 
    django-organizations
    django-rest-framework
    zinnia blog

