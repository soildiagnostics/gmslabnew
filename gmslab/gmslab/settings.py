"""
Django settings for gmslab project.

Generated by 'django-admin startproject' using Django 2.0.6.

For more information on this file, see
https://docs.djangoproject.com/en/2.0/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/2.0/ref/settings/
"""

import os
import django_heroku
from django.contrib.messages import constants as messages

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.0/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'm*3)_3(gr%@^ly5-@$a^xapi#j8)e-@ui_&reg^^h#eq@f07y+'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True  # default is true. Look for production settings below.
ADMINS = [('SoilDx', 'admin@soildiagnostics.com')]
SITE_ID = 1  # added for Zinnia blog

# mo

ALLOWED_HOSTS = [".herokuapp.com", ".gmslab.com", "localhost"]


# Application definition

INSTALLED_APPS = [
    # Standard list
    'grappelli',
    'grappelli.dashboard',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.sites',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.gis',
    'django_extensions',
    'organizations',
    'debug_toolbar',
    'storages',
    'rest_framework',
    #'corsheaders',
    #'crispy_forms',
    #'django_rq',
    'django_comments',
    'mptt',
    'tagging',
    'zinnia',
    'address', ## Third party provider 
    # Standard list ends
    ## SoilDiagnostics Core Apps list
    'contactfields',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    #'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    #'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'debug_toolbar.middleware.DebugToolbarMiddleware',
]

ROOT_URLCONF = 'gmslab.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                #'templates.admincontext.profilepicture',
                'zinnia.context_processors.version', 
            ],
        },
    },
]

WSGI_APPLICATION = 'gmslab.wsgi.application'
# Grappelli settings
GRAPPELLI_INDEX_DASHBOARD = 'gmslab.dashboard.CustomIndexDashboard'
GRAPPELLI_ADMIN_TITLE = 'GMS Laboratories Admin'

# Database
# https://docs.djangoproject.com/en/2.0/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': 'postgres',
        'USER': 'postgres',
        'PASSWORD': 'postgres',
        #'HOST': 'db',
        'HOST' : 'localhost',
        #'PORT': '5432',
        'PORT': '32768',
    }
}
if os.environ.get('DOCKER', False):
    ## Different database settings are permitted 
    DATABASES = {
        'default': {
            'ENGINE': 'django.contrib.gis.db.backends.postgis',
            'NAME': 'postgres',
            'USER': 'postgres',
            'PASSWORD': 'postgres',
            'HOST': 'db',
            #'HOST' : 'localhost',
            'PORT': '5432',
            #'PORT': '32768',
        }
    }

# Password validation
# https://docs.djangoproject.com/en/2.0/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/2.0/topics/i18n/

LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'America/Chicago'
USE_I18N = True
USE_L10N = True
USE_TZ = True

PROJECT_ROOT = os.path.dirname(os.path.abspath(__file__))

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.0/howto/static-files/

# STATIC_URL = '/static/'
# STATIC_ROOT = os.path.join(BASE_DIR, "localstatic")

# STATICFILES_DIRS = [
#     os.path.join(BASE_DIR, "static"),
#     os.path.join(BASE_DIR, "media"),
# ]

# Django-registration settings
ACCOUNT_ACTIVATION_DAYS = 7
DEFAULT_FROM_EMAIL = 'support@gmslab.com'
EMAIL_HOST_USER = "admin@gmslab.com"
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

# Changes by KDB
# Amazon S3 settings
AWS_ACCESS_KEY_ID = 'AKIAIOB3CKQVL7ZTPTAA'
AWS_SECRET_ACCESS_KEY = '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'
AWS_STORAGE_BUCKET_NAME = "gmslab-dev"

# CORS_ORIGIN_ALLOW_ALL = True
if os.environ.get('STAGING', False):
    django_heroku.settings(locals(), staticfiles=False)

if os.environ.get('PRODUCTION', False):
    # CORS_ORIGIN_ALLOW_ALL = False
    DEBUG = False
    #DATABASES['default'] = dj_database_url.config()
    #DATABASES['default']['ENGINE'] = 'django.contrib.gis.db.backends.postgis'

    # AWS_QUERYSTRING_AUTH = False
    AWS_ACCESS_KEY_ID = 'AKIAJF25QKGEIQVAJADQ'
    AWS_SECRET_ACCESS_KEY = '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'

    AWS_STORAGE_BUCKET_NAME = "gmslab-prod"

    SECURE_SSL_REDIRECT = True
    SECURE_PROXY_SSL_HEADER = ("HTTP_X_FORWARDED_PROTO", "https")
    SESSION_COOKIE_SECURE = True
    CSRF_COOKIE_SECURE = True
    SECURE_HSTS_SECONDS = 31536000
    SECURE_HSTS_INCLUDE_SUBDOMAINS = True

    EMAIL_BACKEND = 'sparkpost.django.email_backend.SparkPostEmailBackend'
    django_heroku.settings(locals(), staticfiles=False)

## Set up S3
# ## Added for development
DEFAULT_FILES_STORAGE = 'storages.backends.s3boto3.S3Boto3Storage'
STATICFILES_LOCATION = 'static'
STATICFILES_STORAGE = 'gmslab.custom_storages.StaticStorage'
AWS_S3_CUSTOM_DOMAIN = '%s.s3.amazonaws.com' % AWS_STORAGE_BUCKET_NAME
STATIC_URL = "https://%s/%s/" % (AWS_S3_CUSTOM_DOMAIN,
                                 STATICFILES_LOCATION)

MEDIAFILES_LOCATION = 'media'
MEDIA_URL = "https://%s/%s/" % (AWS_S3_CUSTOM_DOMAIN, MEDIAFILES_LOCATION)
DEFAULT_FILE_STORAGE = 'gmslab.custom_storages.MediaStorage'
AWS_PRELOAD_METADATA = True
# ## end add for development

# Mapbox and Leaflet config

#MAPBOX_KEY = 'pk.eyJ1Ijoic29pbGR4aW5jIiwiYSI6ImNpajkzN2lxODAwMjF1Ym00dmxwb2owZXMifQ.vg7xdzuENvYJw1ETKbexYw'

# LEAFLET_CONFIG = {
#     'DEFAULT_CENTER': (40.111951, -88.230055),  # Urbana!
#     'DEFAULT_ZOOM': 11,
#     'MIN_ZOOM': 3,
#     'MAX_ZOOM': 18,
#     'NO_GLOBALS': False,
# }

#LOGIN_REDIRECT_URL = '/dashboard/'


domain = {
    "domain": "gmslab.com",
    "name": "gmslab.com"
}


INTERNAL_IPS = ('127.0.0.1', '10.0.2.2',)


CORS_ORIGIN_WHITELIST = (
    'localhost:8000',
    'gmslab.com',
    'www.gmslab.com',
)


RQ_QUEUES = {
    'default': {
        'HOST': 'localhost',
        'PORT': '6379',
        # If you're
        'URL': os.getenv('REDISTOGO_URL', 'redis://localhost:6379'),
        'DB': 0,
        'DEFAULT_TIMEOUT': 480,
    },
    'high': {
        'HOST': 'localhost',
        'PORT': '6379',
        # If you're
        'URL': os.getenv('REDISTOGO_URL', 'redis://localhost:6379'),
        'DB': 0,
        'DEFAULT_TIMEOUT': 480,
    },
    'low': {
        'HOST': 'localhost',
        'PORT': '6379',
        # If you're
        'URL': os.getenv('REDISTOGO_URL', 'redis://localhost:6379'),
        'DB': 0,
        'DEFAULT_TIMEOUT': 480,
    }
}

SERIALIZATION_MODULES = {
        "custom_geojson": "gmsrecs.geojson_serializer",
}

CRISPY_TEMPLATE_PACK = 'bootstrap3'
CRISPY_FAIL_SILENTLY = not DEBUG

MESSAGE_TAGS = {
    messages.DEBUG: 'alert-info',
    messages.INFO: 'alert-info',
    messages.SUCCESS: 'alert-success',
    messages.WARNING: 'alert-warning',
    messages.ERROR: 'alert-danger',
}

GOOGLE_MAPS_API_KEY = "AIzaSyBKLhTqk1vvsnovGTaeJ8mpGaPB5ZX3wfA"
