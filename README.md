# GMSLab Django Website (2018)

## Setup for Developers

Install Python 3

Install Git

Install Docker

Create a working folder (GMSWebProject)

Create a virtual environment for Python3

```bash
> python3 -m venv gmslab
```

Activate the virutal environment

```bash
> cd gmslab
> source bin/activate
```

Check that the virtual environment python is active

```bash
> which python
  /Users/kdb_uiuc/CloudHome/GMSWebProject/gmslab/bin/python
```

Upgrade pip if necessary 

```bash
> pip install --upgrade pip
```

Clone the project repo and enter the repo folder

```bash
> git clone git@bitbucket.org:soildiagnostics/gmslabnew.git
> cd gmslabnew

```

Build the docker image

```bash
> docker-compose build 
```

Fire up the image 

```bash
> docker-compose up 
```

When you want to rebuild the image. *Note - this will destroy the database*

```bash
> docker-compose down && docker-compose build 
```

You can rebuild only the web piece like so:

```bash
> docker-compose build web
```

This should prevent the database from being rebuilt


To restart the docker images while retaining the data

```bash
> docker-compose restart
```

The server is set up so that changes made to the Django application should automatically cause the server to restart. 

## Other helpful downloads

1. pgAdmin 4. The postgis instance in docker-compose is exposed to `localhost:32768`. You should be able to point pgAdmin there to look at the database directly. 
2. Kitematic to view and manage Docker containers
