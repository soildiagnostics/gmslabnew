# Dockerfile 
# for GMSLab website

FROM python:3
ENV PYTHONUNBUFFERED 1 
ENV DOCKER 1

RUN mkdir -p /code
WORKDIR /code

ENV DJANGO_SETTINGS_MODULE=gmslab.settings \
    PYTHONPATH=/code 

# debian packages: GeoDjango dependencies
RUN apt-get update && \
    apt-get install -y binutils libproj-dev gdal-bin && \
    rm -rf /var/lib/apt/lists/*

# django apps & manage

COPY requirements.txt /code/requirements.txt
RUN pip install --no-cache-dir -r /code/requirements.txt
COPY . /code/

# Heroku doesn't like this 
EXPOSE 8000

## With docker-compose this one doesn't run. 
CMD gunicorn --chdir '/code/gmslab' gmslab.wsgi:application --bind=0:$PORT 